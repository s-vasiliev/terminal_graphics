// Header: Terminal grafics
// File Name: terminal_grafics.c
// Author: Kargapolcev M. E.
// Date: 26.06.2019
// Based on https://habr.com/ru/post/325082/ (dlinyj 28.03.2017-18:56)
// https://github.com/dlinyj/terminal_controller
// http://microsin.net/adminstuff/xnix/ansivt100-terminal-control-escape-sequences.html
// https://misc.flogisoft.com/bash/tip_colors_and_formatting
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "terminal_graphics.h"
#include "term.h"

#define home()            (printf(ESC "[H")) //Move cursor to the indicated row, column (origin at 1,1)
#define clear()           (printf(ESC "[2J")) //lear the screen, move to (1,1)
#define gotoxy(x,y)       (printf(ESC "[%d;%dH", y, x))
#define visible_cursor()  (printf(ESC "[?251"))
#define resetcolor()      (printf(ESC "[0m"))
#define set_atrib(color)  (printf(ESC "[%dm", color))
#define up()              (printf(ESC "[1A"))
#define down()            (printf(ESC "[1B"))
#define Query_Cur_Pos()   (printf(ESC "[6n"))
#define Query_Dev_Stat()  (printf(ESC "[5n"))
#define Title()           (printf(ESC "[2<STM32 Terminal>\x07"))
#define Bell();           (printf("\x07"))

void terminal_cursor(void);
void terminal_frame(void);
void terminal_header(void);
void terminal_body(void);
void terminal_basement(void);

#define BACK_GROUND       Dark_gray
#define INK               Light_green

// Point on screen
typedef struct {
  uint8_t x;
  uint8_t y;
} point_t;

// Label
typedef struct {
  point_t point;
  uint8_t text[8];
} label_t;
label_t label_debug;
// Switch
typedef enum {
  off,
  on,
}state_t;
typedef struct {
  state_t state;
  point_t point;
  label_t label;
} radbtn_t;
radbtn_t radbtn_led;
radbtn_t radbtn_0;
radbtn_t radbtn_1;
// Cursor section

// Cursor
typedef struct {
  // Position
  point_t pstn;
  // Last position
  point_t last;
}cursor_t;
cursor_t cursor;

static inline void cursor_save(cursor_t *curs);
static inline void cursor_update(cursor_t *curs);
static inline void cursor_set(point_t *point);

static inline void label_draw(label_t *lb);
static inline void radbtn_draw(radbtn_t *sw);
static inline void radbtn_update(radbtn_t *sw);
static inline void radbtn_check(radbtn_t *sw);
static inline void radbtn_toggle(radbtn_t *sw);
// Cursor end

// escape sequence search
bool esc_seq = false;
uint8_t esc_seq_buff[6] = {0,0,0,0,0,0};
uint8_t esc_seq_iter = 0;
typedef enum {
  None,
  Up,
  Down,
  Left,
  Right,
  F1,
  F2,
  F3,
  F4,
  F5,
  F6,
  F7,
  F8,
}esc_code_t;
void esc_seq_clear(void);
esc_code_t esc_seq_search(void);
const uint8_t up[4]     = {0x1b, 0x5b, 0x41, 0x00};
const uint8_t down[4]   = {0x1b, 0x5b, 0x42, 0x00};
const uint8_t right[4]  = {0x1b, 0x5b, 0x43, 0x00};
const uint8_t left[4]   = {0x1b, 0x5b, 0x44, 0x00};
const uint8_t f1[6]     = {0x1b, 0x5b, 0x31, 0x31, 0x7e, 0x00};
const uint8_t f2[6]     = {0x1b, 0x5b, 0x31, 0x32, 0x7e, 0x00};
const uint8_t f3[6]     = {0x1b, 0x5b, 0x31, 0x33, 0x7e, 0x00};
const uint8_t f4[6]     = {0x1b, 0x5b, 0x31, 0x34, 0x7e, 0x00};
const uint8_t f5[6]     = {0x1b, 0x5b, 0x31, 0x35, 0x7e, 0x00};
const uint8_t f6[6]     = {0x1b, 0x5b, 0x31, 0x37, 0x7e, 0x00};
const uint8_t f7[6]     = {0x1b, 0x5b, 0x31, 0x38, 0x7e, 0x00};
const uint8_t f8[6]     = {0x1b, 0x5b, 0x31, 0x39, 0x7e, 0x00};
const uint8_t dev_stat[5] = {0x1b, 0x5b, 0x30, 0x6e, 0x00};

uint8_t buff;
bool terminal_status = false;
bool terminal_redraw = false;
extern UART_HandleTypeDef huart2;

void terminal_init(void)
{
  
  home();
  clear();
  //Title(); Bell();
  terminal_frame();
  // Radio buttons
  radbtn_led.state = off,
  radbtn_led.point.x = 24;
  radbtn_led.point.y = 2;
  memcpy(radbtn_led.label.text, "led ctrl", 8);
  radbtn_led.label.point.x = 24 + 2;
  radbtn_led.label.point.y = 2;
  radbtn_draw(&radbtn_led);
  
  radbtn_0.state = off,
  radbtn_0.point.x = 24;
  radbtn_0.point.y = 5;
  memcpy(radbtn_0.label.text, "radbtn#0", 8);
  radbtn_0.label.point.x = radbtn_0.point.x + 2;
  radbtn_0.label.point.y = radbtn_0.point.y;
  radbtn_draw(&radbtn_0);
  
  radbtn_1.state    = on,
  radbtn_1.point.x  = 24;
  radbtn_1.point.y  = 8;
  radbtn_1.label.point.x = radbtn_1.point.x + 2;
  radbtn_1.label.point.y = radbtn_1.point.y;
  memcpy(radbtn_1.label.text, "radbtn#1", 8);
  radbtn_draw(&radbtn_1);
  // Labels
  label_debug.point.x = 2;
  label_debug.point.y = 9;
  memcpy(label_debug.text, "--------", 8);
  label_draw(&label_debug);
  
  // Cursor start position
  cursor.pstn.x = 2;
  cursor.pstn.y = 4;
  HAL_UART_Receive_DMA(&huart2, RX, 2);
}

void terminal_update(void)
{
  /*Query_Dev_Stat();
  if(terminal_status == false) {
    terminal_redraw = true;
  }*/
  /*if(terminal_redraw) {
      home();
      clear();
      terminal_frame();
  }*/
  
  terminal_cursor();
  buff = esc_seq_search();
  terminal_header();
  terminal_body();
  terminal_basement();
  
  // Labels
  label_draw(&label_debug);
  
  // Radio buttons
  radbtn_update(&radbtn_led);
  if(radbtn_led.state) {
    LD4(LED_ON);
  }else {
    LD4(LED_OFF);
  }
  
  if(blue_button) {
    radbtn_0.state = on;
  }else {
    radbtn_0.state = off;
  }
  radbtn_update(&radbtn_0);
  radbtn_update(&radbtn_1);
  
  
  cursor_update(&cursor);
}

void esc_seq_clear(void)
{
  memset(esc_seq_buff, 0, 6);
  esc_seq = false;
  esc_seq_iter = 0;
}

esc_code_t esc_seq_search(void)
{
  terminal_status = false;
  if(strcmp((char*)esc_seq_buff, (char*)up) == 0) {
    esc_seq_clear();
    return Up;}
  if(strcmp((char*)esc_seq_buff,(char*) down) == 0) {
    esc_seq_clear();
    return Down;}
  if(strcmp((char*)esc_seq_buff, (char*)left) == 0) {
    esc_seq_clear();
    return Left;}
  if(strcmp((char*)esc_seq_buff, (char*)right) == 0) {
    esc_seq_clear();
    return Right;}
  if(strcmp((char*)esc_seq_buff, (char*)f1) == 0) {
    esc_seq_clear();
    return F1;}
  if(strcmp((char*)esc_seq_buff, (char*)f2) == 0) {
    esc_seq_clear();
    return F2;}
  if(strcmp((char*)esc_seq_buff, (char*)f3) == 0) {
    esc_seq_clear();
    return F3;}
  if(strcmp((char*)esc_seq_buff, (char*)f4) == 0) {
    esc_seq_clear();
    return F4;}
  if(strcmp((char*)esc_seq_buff, (char*)f5) == 0) {
    esc_seq_clear();
    return F5;}
  if(strcmp((char*)esc_seq_buff, (char*)f6) == 0) {
    esc_seq_clear();
    return F6;}
  if(strcmp((char*)esc_seq_buff, (char*)f7) == 0) {
    esc_seq_clear();
    return F7;}
  if(strcmp((char*)esc_seq_buff, (char*)f8) == 0) {
    esc_seq_clear();
    Bell();
    return F8;}
  if(strcmp((char*)esc_seq_buff, (char*)dev_stat) == 0) {
    //terminal_status = true;
    //terminal_redraw = false;
    esc_seq_clear();
    return None;}

  return None;
}

// Callback for new byte
void terminal_callback(uint8_t *p_s)
{
  // Escape symbol find
  if(*p_s == 0x1b) {
    //new escape sequence
    esc_seq = true;
    esc_seq_buff[esc_seq_iter++] = *p_s;
  }else {
    if(esc_seq) {
      esc_seq_buff[esc_seq_iter++] = *p_s;
      if(esc_seq_iter > 5) {
        esc_seq_clear();
      }
      //buff = esc_seq_search();
      return;
    }
    // data symbols
    buff = *p_s;
  }
}

void terminal_cursor(void)
{
  switch(buff)
  {
    // Nothing to do
    case 0x00:
      break;
    // Cursor keys
    case Up:
      cursor.pstn.y--;
      break;
    case Down:
      cursor.pstn.y++;
      break;
    case Left:
      cursor.pstn.x--;
      break;
    case Right:
      cursor.pstn.x++;
      break;
    // Func keys
    case F1:
      memcpy(label_debug.text, "F1 key  ", 8);
      break;
    case F2:
      memcpy(label_debug.text, "F2 key  ", 8);
      break;
    case F3:
      memcpy(label_debug.text, "F3 key  ", 8);
      break;
    case F4:
      memcpy(label_debug.text, "F4 key  ", 8);
      break;
    case F5:
      memcpy(label_debug.text, "F5 key  ", 8);
      break;
    case F6:
      memcpy(label_debug.text, "F6 key  ", 8);
      break;
    case F7:
      memcpy(label_debug.text, "F7 key  ", 8);
      break;
    case F8:
      memcpy(label_debug.text, "F8 key  ", 8);
      Bell();
      break;

    // Space
    case ' ':
      //printf("█");
      radbtn_check(&radbtn_led);
      radbtn_check(&radbtn_0);
      radbtn_check(&radbtn_1);
      break;
    default:
      break;
  }
  buff = 0;
}

// Frame for GUI
void terminal_frame(void)
{
  set_atrib(BACK_GROUND);
  set_atrib(F_GREEN);
  clear();
  /* Symbol libriary */
  /* └ ┘ ┌ ┐ ─ │ ├ ┤ */
  /*
2500  ─ ━ 	│ 	┃ 	┄ 	┅ 	┆ 	┇ 	┈ 	┉ 	┊ 	┋ 	┌ 	┍ 	┎ 	┏
2510   ┐ 	┑ 	┒ 	┓ 	└ 	┕ 	┖ 	┗ 	┘ 	┙ 	┚ 	┛ 	├ 	┝ 	┞ 	┟
2520   ┠ 	┡ 	┢ 	┣ 	┤ 	┥ 	┦ 	┧ 	┨ 	┩ 	┪ 	┫ 	┬ 	┭ 	┮ 	┯
2530   ┰ 	┱ 	┲ 	┳ 	┴ 	┵ 	┶ 	┷ 	┸ 	┹ 	┺ 	┻ 	┼ 	┽ 	┾ 	┿
2540   ╀ 	╁ 	╂ 	╃ 	╄ 	╅ 	╆ 	╇ 	╈ 	╉ 	╊ 	╋ 	╌ 	╍ 	╎ 	╏
2550   ═ 	║ 	╒ 	╓ 	╔ 	╕ 	╖ 	╗ 	╘ 	╙ 	╚ 	╛ 	╜ 	╝ 	╞ 	╟
2560   ╠ 	╡ 	╢ 	╣ 	╤ 	╥ 	╦ 	╧ 	╨ 	╩ 	╪ 	╫ 	╬ 	╭ 	╮ 	╯
2570   ╰ 	╱ 	╲ 	╳ 	╴ 	╵ 	╶ 	╷ 	╸ 	╹ 	╺ 	╻ 	╼ 	╽ 	╾ 	╿
2580   ▀ 	▁ 	▂ 	▃ 	▄ 	▅ 	▆ 	▇ 	█ 	▉ 	▊ 	▋ 	▌ 	▍ 	▎ 	▏
2590   ▐ 	░ 	▒ 	▓ 	▔ 	▕ 	▖ 	▗ 	▘ 	▙ 	▚ 	▛ 	▜ 	▝ 	▞ 	▟*/
  
  //     0123456789
  puts( "┌────────────────────┐\n\r" //0
        "│                    │\n\r" //1 Header
        "├─┬──────────────────┤\n\r" //2
        "│ │                  │\n\r" //3
        "├─┤                  │\n\r" //4
        "│ │                  │\n\r" //5
        "├─┘                  │\n\r" //6
        "│                    │\n\r" //7
        "│                    │\n\r" //8
        "└────────────────────┘\n\r" //9
        "┌────────────────────┐\n\r" //10
        "│                    │\n\r" //11
        "│                    │\n\r" //12
        "└────────────────────┘");   //13
  //resetcolor();
}

void terminal_header(void)
{
  gotoxy(2,2);
  set_atrib(BACK_GROUND);
  set_atrib(INK);
  printf("Header");
  //resetcolor();
}

void terminal_body(void)
{
  
}

void terminal_basement(void)
{
  set_atrib(BACK_GROUND);
  set_atrib(INK);
  gotoxy(2, 12);
  printf("cursor.pstn.x: %d  ", cursor.pstn.x);
  gotoxy(2, 13);
  printf("cursor.pstn.y: %d  ", cursor.pstn.y);
}
// Cursor
static inline void cursor_update(cursor_t *curs)
{
  cursor_set(&curs->pstn);
  cursor_save(curs);
}

static inline void cursor_save(cursor_t *curs)
{
  if(curs->pstn.x == curs->last.x) {
    curs->last.x = curs->pstn.x;
  }
  if(curs->pstn.y == curs->last.y) {
    curs->last.y = curs->pstn.y;
  }
}

static inline void cursor_set(point_t *point)
{
  gotoxy(point->x , point->y);
}
// Labels
static inline void label_draw(label_t *lb)
{
  cursor_set(&lb->point);
  printf("%s", lb->text);
  //cursor_set(&cursor.pstn);
}
static inline void radbtn_draw(radbtn_t *sw)
{
  cursor_set(&sw->point);
  label_draw(&sw->label);
  point_t temp;
  temp.x = sw->point.x - 1;
  temp.y = sw->point.y - 1;
  cursor_set(&temp);
  printf("%s", "┌─┐");
  temp.x = sw->point.x - 1;
  temp.y = sw->point.y;
  cursor_set(&temp);
  printf("%s", "│ │");
  temp.x = sw->point.x - 1;
  temp.y = sw->point.y + 1;
  cursor_set(&temp);
  printf("%s", "└─┘");
  //cursor_set(&cursor.pstn);
}

static inline void radbtn_update(radbtn_t *sw)
{
  cursor_set(&sw->point);
  if(sw->state) {
    printf("%s", "█");
  }else {
    printf("%s", " ");
  }
}

static inline void radbtn_check(radbtn_t *sw)
{
  if(sw->point.x == cursor.pstn.x &&\
   sw->point.y == cursor.pstn.y) {
    radbtn_toggle(sw);
  }
}

static inline void radbtn_toggle(radbtn_t *sw)
{
  sw->state = !sw->state;
}
